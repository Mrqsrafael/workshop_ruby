## [WORKSHOP] Ruby

## Introduction au Ruby


```ruby
class Car

  # there is 3 attr types 
  # reader make the variable readable
  # writer make the variable modifiable
  # accessor permit both

  attr_accessor :fuel, :name 

    # the initialize function is called when the class is created
    def initialize(fuel = 100, name = "default_name")
        @fuel = fuel
        @name = name
    end

    def drive()
        # we remove 5 from the car's fuel
       @fuel -= 5
    end

    def change_name(new_name)
        @name = new_name

    end
end
# create an instance of the car class
Toyota = Car.new(100, "Toytota 500")

# call the function "drive"
Toyota.drive()

# call the function "change_name"
Toyota.change_name("Peugeot 306")

puts(Toyota.name)

# display the car's fuel
puts(Toyota.fuel)

# if condition:
if Toyota.fuel > 90
  puts('true')
end

# while condition:
while Toyota.fuel > 50
  Toyota.drive()
end

puts(Toyota.fuel)

for x in (1..5)
  puts(x)
end

array = [1, 2, 3, 4, 5, 6]
array.length
#return the array length
array.empty?
#return true or false depending if the array is empty or not
array.push(7)
#push add an element in the array

for i in array
  print i
end

```

### Exercice 1

Créez une classe Monstre ayant des caractéristiques par défaults:
- HP: 50
- ATK: 5
- NOM: "MONSTRE"

Créez ensuite une fonction qui lui retirera de la vie par rapport au paramètre passé.


## L'héritage en Ruby

```ruby
class Mammal  

  attr_accessor :legs

  def initialize(nb = 2)
    @legs = nb
  end

  def breathe  
    puts "inhale and exhale"  
  end  
end  
  
class Cat < Mammal  

  # super call the Mammal initialize
  def initialize(nb = 4) super
      puts @legs
  end

  def speak  
    puts "Meow"
  end
end  

rani = Cat.new  
rani.breathe  
rani.speak  
 
```

### Exercice 2

Créez 2 nouvelles classes de monstre héritant de votre classe crée précédemment

Gobelin :
- HP: 30
- ATK: 5

SQUELETTE:
- HP: 60
- ATK: 10

### Exercice 3

Maintenant que vous avez crée plusieurs monstres, vous pouvez créer votre personnage avec plusieurs classes

Aventurier(Classe de base) :
- HP: 80
- ATK: 18
- Mana: 30

Guerrier:
- HP: 100
- ATK: 20
- Mana: 10

Archer:
- HP: 70
- ATK: 15
- Mana: 20

### Exercice 4

Vous allez maintenant rajouter une compétence spéciale à chaque classe :

Aventurier :
- augmente l'atk de 15 pour les deux prochaines attaques, consomme 10 de mana

Guerrier:
- une attaque ayant 25 d'atk et régènere le guerrier de 10, consomme 5 de mana

Archer:
- inflige 30 degats, consome 10 de mana


### Exercice 5

Créez une classe game qui possède votre classe Personnage et Monstre

## Récuper les inputs de l'utilisateur

```ruby
puts "Enter your name: "
#gets get the \n from enter
name = gets
puts "Hello #{name}, how are you"

puts "Enter your name: "
#gets.chomp cut this \n
name = gets.chomp
puts "Hello #{name}, how are you"

```

### Exercice 6

Faites une sélection de classe pour votre personnage principal au lancement du jeu

### Exercice 7

Faites un premier combat entre votre personnage et le monstre de votre choix en tour par tour.
Le monstre attaquera toujours mais le joueur pourra
- Faire une attaque normale 
- Faire son attaque spéciale
- Fuir.

### Exercice 8

Maintenant que vous un combat basique, vous pouvez le complexifier avec des objets que votre personnage possédera.
Créez donc plusieurs classes :

Faible Potion de soin:
- Restaure 20HP

Faible Potion de Force:
- Augmente l'atk de 10

### Exercice 9

#### A vous de jouer, maintenant que votre système de combat est fait vous pouvez vous pencher sur le scnéario etc...




